import streamlit as st
import pandas as pd
import datetime as datetime
import pyodbc


# Initialize connection.
# Uses st.experimental_singleton to only run once.
@st.experimental_singleton
def init_connection():
    return pyodbc.connect(
        "DRIVER={ODBC Driver 17 for SQL Server};SERVER="
        + st.secrets["server"]
        + ";DATABASE="
        + st.secrets["database"]
        + ";UID="
        + st.secrets["username"]
        + ";PWD="
        + st.secrets["password"]
    )

# Perform query.
# Uses st.experimental_memo to only rerun when the query changes or after 10 min.
@st.experimental_memo(ttl=600)
def run_query(query):
    with conn.cursor() as cur:
        cur.execute(query)
        return cur.fetchall()

with init_connection() as conn:
    with conn.cursor() as cursor:
        cursor.execute("SELECT TOP 3 name, collation_name FROM sys.databases")
        row = cursor.fetchone()
        while row:
            st.write(str(row[0]) + " " + str(row[1]))
            row = cursor.fetchone()

with st.container():
    dataset = st.selectbox('What dataset do you wish to access?',('', 'Referrals', 'Contacts', 'D2A'))
    if not dataset:
        st.warning('Please select a dataset')
        st.stop()

    localities = st.multiselect(
        'Which localities',
        ['North', 'South', 'West', 'Norwich','SSOCS'],
        [])

    if not localities:
        st.warning('Please select a locality')
        st.stop()

    date_col1, date_col2 = st.columns(2)
    start_date = date_col1.date_input("Date From",datetime.date(2019, 7, 6))
    end_date = date_col2.date_input("Date To")

    if not start_date and not end_date:
        st.warning('Please select a date range')
        st.stop()


    service_col1, service_col2 = st.columns(2)

    core_service = service_col1.selectbox('Core Service',('', 'Community Nursing & Therapy', 'Continence'))
    if not core_service:
        st.warning('Please select a Core Service')
        st.stop()

    service_offered = service_col2.multiselect(
        'Service Offered',
        ['CN&T - Nursing', 'CN&T - OT', 'CN&T - Physiotherapy'],
        [])




